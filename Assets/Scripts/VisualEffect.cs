﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualEffect : MonoBehaviour {
    public GameObject quizBox,AnswerBox;
    Vector2 startPos;
    public Vector2 movePos;
    public Vector2 currentPos;
    public float speed;
    public bool executeMove = false;
	// Use this for initialization

	
	// Update is called once per frame
	void Start () {
        startPos = quizBox.transform.position;
        currentPos = startPos;
        executeMove = false;
	}

    void Update()
    {
        if (executeMove) {
            if (currentPos.x > -800)
            {
                Debug.Log("moooove");
                quizBox.transform.Translate((speed * Time.deltaTime) * -1, 0, 0);
                AnswerBox.transform.Translate((speed * Time.deltaTime) * 1, 0, 0);
                currentPos = quizBox.transform.position;
            }
            else {
                executeMove = false;
            }
        }
    }

}
