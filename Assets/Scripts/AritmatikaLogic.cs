﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class AritmatikaLogic : MonoBehaviour {
    public Vector3 mousePos;
    public int answer;
    public int num_1,num_2,num_3;
    public List<GameObject> answerText;
    public GameObject num1_obj, num2_obj, op_obj,trueBox;
    public Sprite[] _num;
    public List<string> jawaban = new List<string> (3);
    // Use this for initialization

    void Start()
    {
        callNum();
     //   quizGenerator();
    }

    public void callNum() {
    _num = Resources.LoadAll<Sprite>("number_dummy");
    quizGenerator();
    }

    private void quizGenerator() {
        //callNum();
        int _num_1 = numberGenerator(0, 10);
        int _num_2 = numberGenerator(0, 10);
        int _operand = numberGenerator(0,2);
        num_3 = _operand;
        if (_operand == 0)
        {
            if (_num_1 < _num_2)
            {
                _num_1 = numberGenerator(_num_2, 10);
                num_1 = _num_1;
                num_2 = _num_2;
                answer = _num_1 - _num_2;
                Debug.Log(num_1);
                Debug.Log(num_2);
                Debug.Log(answer);
            }
            else
            {
                num_1 = _num_1;
                num_2 = _num_2;
                answer = _num_1 - _num_2;
                Debug.Log(num_1);
                Debug.Log(num_2);
                Debug.Log(answer);
            }
        }
        else if (_operand == 1) {
            num_1 = _num_1;
            num_2 = _num_2;
            num_3 = _operand;
            answer = _num_2 + _num_1;
            Debug.Log(num_1);
            Debug.Log(num_2);
            Debug.Log(num_3);
            Debug.Log(answer);
        }
        trueBox.GetComponent<SlotAnswer>().rightAnswer = answer;
        randAnswerNum();
    }

    private int numberGenerator(int min, int max) {
        return Random.Range(min, max);
    }

    public void randAnswerNum() {
        jawaban[0] = answer.ToString();
        jawaban[1] = numberGenerator(answer+1, 20).ToString();
        jawaban[2] = numberGenerator(int.Parse(jawaban[1])+1, 22).ToString();
        AssignAnswer();
    }

    private void AssignAnswer() {
        List<GameObject> obj = new List<GameObject>(answerText);
        int i = 0;
        while (obj.Count > 0) {
            int num = numberGenerator(0, obj.Count);
            obj[num].GetComponentInChildren<Text>().text = jawaban[i];
            obj[num].GetComponentInChildren<DragDrop>().nil = int.Parse(jawaban[i]);
            obj.RemoveAt(num);
            i++;
        }
        //      answerText = new List<GameObject>(obj);
        AssignGameObject();
    }

    public void AssignGameObject() {
        num1_obj.GetComponent<Image>().sprite = _num[num_1];
        num2_obj.GetComponent<Image>().sprite = _num[num_2];
        if (num_3 == 0)
        {
            op_obj.GetComponent<Image>().sprite = _num[10];
        }
        else {
            op_obj.GetComponent<Image>().sprite = _num[12];
        }
    }
}
