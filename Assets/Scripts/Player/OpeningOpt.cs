﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpeningOpt : MonoBehaviour {

    public int wrapIntro;
    public GameObject Tint;
    // Update is called once per frame
    private void Start()
    {
        wrapIntro = PlayerData.Instance.videoIntro;
        GetComponent<MoviePlayer>().StartPlay();
    }

    void Update() {
        //if (wrapIntro == 0)
        //{
        //    gameObject.GetComponent<MoviePlayer>().StartPlay();
            if (Input.GetMouseButton(0) || Input.GetKey(KeyCode.Space))
            {
                //skip
                gameObject.GetComponent<MoviePlayer>().stopPlay();
                PlayerData.Instance.videoIntro = 1;
                wrapIntro++;
                startTheScreen();
            }
        //}
        //else if (wrapIntro >= 1) {
        //    startTheScreen();
        //}
    }

    public void startTheScreen() {
        gameObject.GetComponent<AudioSource>().Play();
        StartCoroutine(tintChange());
    }

    IEnumerator tintChange() {
        float timer = 0f;
        float _r = 0;
        float _g = 0;
        float _b = 0;
        float _a = 2f;
        while (_r <= 2f) {
            Debug.Log(_r);
            yield return new WaitForSeconds(0.01f);
            Tint.GetComponent<Image>().color = new Color(_r+=Time.deltaTime, _g+=Time.deltaTime, _b+=Time.deltaTime, _a);
        }
        Debug.Log("alpha change");
        yield return new WaitForSeconds(1f);
        while (_a >= 0f) {
            yield return new WaitForSeconds(0.01f);
            Tint.GetComponent<Image>().color = new Color(_r, _g, _b, _a-=Time.deltaTime*2);
        }
        yield return new WaitForSeconds(0.3f);
        Tint.SetActive(false);

        StopCoroutine(tintChange());
    }
}
