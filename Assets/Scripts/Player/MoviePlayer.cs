﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class MoviePlayer : MonoBehaviour { 
    public string name;
    public GameObject videoPlayer;
    private VideoPlayer pl;
    // Use this for initialization

    void Start()
    {
     //   pl.url = System.IO.Path.Combine(Application.streamingAssetsPath, name);
        //videoPlayer.GetComponent<VideoPlayer>().url = System.IO.Path.Combine(Application.streamingAssetsPath, name);
    }
        public void StartPlay() {
        videoPlayer.GetComponent<VideoPlayer>().Play();
    }

    public void stopPlay() {
        videoPlayer.GetComponent<VideoPlayer>().Stop();
    }

    public bool getLength() {
        return videoPlayer.GetComponent<VideoPlayer>().isPlaying;
    }
}
