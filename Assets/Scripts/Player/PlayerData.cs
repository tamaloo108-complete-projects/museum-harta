﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour {
    public int savedata,levelAvailability,MiniGameAvailability,Score, miniGameCounter, currentScore, health, currentHealth;
    public bool isGagal;
    public int timer;
    public int videoIntro;
    private static PlayerData instance;
    public static PlayerData Instance { get { return instance; } }

    void Awake()
    {
        videoIntro = 0;
        GameObject[] count = GameObject.FindGameObjectsWithTag("Data");
        Debug.Log(count.Length);
        if (count.Length == 1) { 
        DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(count[count.Length - 1]);
        }
        instance = this;
        if (PlayerPrefs.HasKey("savedata"))
        {
            //continue
            savedata = PlayerPrefs.GetInt("savedata");
            levelAvailability = PlayerPrefs.GetInt("levelAvailability");
            MiniGameAvailability = PlayerPrefs.GetInt("MiniGameAvailability");
            PlayerPrefs.SetInt("Health", 5);
            PlayerPrefs.SetInt("score", 0);
            timer = PlayerPrefs.GetInt("timer");

           // health = PlayerPrefs.GetInt("Health");
           // Score = PlayerPrefs.GetInt("score");
        }
        else {
            //new game
           // videoIntro = 0;
            PlayerPrefs.SetInt("savedata", savedata);
            PlayerPrefs.SetInt("levelAvailability",levelAvailability);
            PlayerPrefs.SetInt("MiniGameAvailability",MiniGameAvailability);
            PlayerPrefs.SetInt("score",Score);
            PlayerPrefs.SetInt("Health", health);
            PlayerPrefs.SetInt("CurrentHealth", currentHealth);
            PlayerPrefs.SetInt("CurrentScore", currentScore);
            PlayerPrefs.SetFloat("MiniGameCounter", miniGameCounter);
            PlayerPrefs.SetInt("timer", timer);
        }
    }


    public void UnlockNewLevel(int lvl) {
        PlayerPrefs.SetInt("levelAvailability", lvl);
    }

    public void UnlockNewMiniGame(int nil) {
        PlayerPrefs.SetInt("MiniGameAvailability",nil);
    }
    public void setScore(int nil)
    {
        PlayerPrefs.SetInt("score", nil);
    }

    public int getScore() {
        return PlayerPrefs.GetInt("score");
    }

    public void setHealth(int nil) {
        PlayerPrefs.SetInt("Health", nil);
    }

    public int getHealth() {
        return PlayerPrefs.GetInt("Health");
    }

    public int getTimer() {
        return PlayerPrefs.GetInt("timer");
    }

    public void setTimer(int nil) {
        PlayerPrefs.SetInt("timer", nil);
    }

    public void setMiniGame(float nil) {
        PlayerPrefs.SetFloat("MiniGameCounter", nil);
    }

    public void resetAll() {
        videoIntro = 0;
        PlayerPrefs.SetInt("savedata", 0);
        PlayerPrefs.SetInt("levelAvailability", 0);
        PlayerPrefs.SetInt("MiniGameAvailability", 0);
        PlayerPrefs.SetInt("score", 0);
        PlayerPrefs.SetInt("Health", 5);
        PlayerPrefs.SetInt("timer", timer);
    }
}
