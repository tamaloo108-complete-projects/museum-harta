﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelGrouping : MonoBehaviour
{

    public GameObject levelPrefab;
    public GameObject levelPrefabParent;
    public GameObject loadingScreen;
    void Start()
    {
        loadSpriteLevel();
    }

    private void loadSpriteLevel()
    {
        Sprite[] thumbnails = Resources.LoadAll<Sprite>("matching_game");
        int nil = 0;
        foreach (Sprite x in thumbnails)
        {
            GameObject container = Instantiate(levelPrefab) as GameObject;
            container.GetComponent<Image>().sprite = x;
            container.transform.SetParent(levelPrefabParent.transform, false);
            string lvl = container.GetComponent<CardIdentity>().level = "level_0" + nil;
            if (nil <= PlayerData.Instance.levelAvailability)
            {
                container.GetComponent<Button>().onClick.AddListener(() => loadLevel(lvl));
                nil++;
            }
        }
    }

    private void loadLevel(string lvl)
    {

        loadingScreen.SetActive(true);
        StartCoroutine(videoDur(lvl));
    }

    IEnumerator videoDur(string lvl)
    {
        GetComponent<MoviePlayer>().StartPlay();
        yield return new WaitForSeconds(3f);
        AsyncOperation loadingBoi = SceneManager.LoadSceneAsync(lvl);
        float proc = loadingBoi.progress;
        if (loadingBoi.isDone)
        {
            yield return new WaitForSeconds(2f);
            GetComponent<MoviePlayer>().stopPlay();
            //gameObject.GetComponent<MoviePlayer>().stopPlay();
            loadingScreen.SetActive(false);
            StopCoroutine(videoDur(lvl));
        }

    }
}
