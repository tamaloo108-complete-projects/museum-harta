﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnWhite : MonoBehaviour {
    public Transform enemyPrefab;
    public Transform spawnPoint;
    private float countdown = 0f;
    public float timeBetweenWaves;
    public int waveIndex;
    public GameObject manager;

    void Update()
    {
        if (manager.GetComponent<GameLogic>().isPlaying)
        {
            if (countdown <= 0f)
            {
                //release the kraken!
                StartCoroutine(spawnWave());
                countdown = timeBetweenWaves;

            }
            countdown -= Time.deltaTime;
        }
    }

    IEnumerator spawnWave()
    {
        // waveIndex++;
        for (int i = 0; i < waveIndex; i++)
        {
            spawnEnemy();
            yield return new WaitForSeconds(0.4f);
        }


    }

    public void spawnEnemy()
    {
        Instantiate(enemyPrefab, spawnPoint.position, spawnPoint.rotation);

    }
}
