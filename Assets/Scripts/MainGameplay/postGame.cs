﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class postGame : MonoBehaviour
{
    public bool needStart, needStop;

    public GameObject img, scoring, backScoring;
    public GameObject loading, txtCongrats,angka, btnCon, btnBack, grats;
    public GameObject[] bintang;
    public string lvl;
    IEnumerator instance;
    // Use this for initialization

    public void postGameStart()
    {
       // StartCoroutine(LoadBlackScreen(2.5f));
        if (!gameObject.GetComponent<GameLogic>().isPlaying)
        {
            img.SetActive(true);
            StartCoroutine(LoadBlackScreen());
            
        }
    }


    public IEnumerator finaleGame() {
        StopCoroutine(LoadBlackScreen());
        while (loading.GetComponent<Image>().fillAmount != 1)
        {
            yield return new WaitForSeconds(0.01f);
            loading.GetComponent<Image>().fillAmount += Time.deltaTime;
        }
        StopCoroutine(LoadBlackScreen());
        yield return new WaitForSeconds(2f);
        gameObject.GetComponent<MoviePlayer>().StartPlay();
        yield return gameObject.GetComponent<MoviePlayer>().getLength();
        gameObject.GetComponent<MoviePlayer>().stopPlay();
        SceneManager.LoadScene(lvl);

    }

    IEnumerator LoadBlackScreen() {
        StopCoroutine(LoadBlackScreen());
            while (loading.GetComponent<Image>().fillAmount != 1) {
                yield return new WaitForSeconds(0.01f);
            loading.GetComponent<Image>().fillAmount += Time.deltaTime;
            }
        StopCoroutine(LoadBlackScreen());
        yield return new WaitForSeconds(2f);
        StartCoroutine(UnloadBlackScreen());
    }


    IEnumerator UnloadBlackScreen() {
        StopCoroutine(UnloadBlackScreen());
        scoring.SetActive(true);
        while (loading.GetComponent<Image>().fillAmount != 0)
        {
            yield return new WaitForSeconds(0.01f);
            loading.GetComponent<Image>().fillAmount -= Time.deltaTime;
        }
        StopCoroutine(UnloadBlackScreen());
        StartCoroutine(berhasilAnim());
    }

    IEnumerator berhasilAnim() {
        if (PlayerData.Instance.isGagal == false)
        {
            grats.GetComponent<Text>().text = "Berhasil!";
        }
        else if (PlayerData.Instance.isGagal == true) {

            grats.GetComponent<Text>().text = "Gagal";
        }
        grats.SetActive(true);
        float timer = 0f;
        float form1 = 0f;
        while (timer < 3f) {

                if (form1 <= 1f)
                {
                    yield return new WaitForSeconds(0.01f);
                    grats.GetComponent<RectTransform>().localScale = new Vector3(form1, 1, 1);
                    form1 += Time.deltaTime * 3f;
                }
            timer += Time.deltaTime;
        }

        StopCoroutine(berhasilAnim());
        StartCoroutine(calculateScoreNum());
    }


    IEnumerator calculateScoreNum() {
        Debug.Log(gameObject.GetComponent<GameLogic>().totalScore.ToString());
        float timer = 2f;
        while (timer > 1f) {
            yield return new WaitForSeconds(0.01f);
            angka.GetComponent<Text>().text = Random.Range(0, 999).ToString();
            timer -= Time.deltaTime;
        }
            yield return new WaitForSeconds(0.01f);
            angka.GetComponent<Text>().text = gameObject.GetComponent<GameLogic>().totalScore.ToString();
        StopCoroutine(calculateScoreNum());
        if (PlayerData.Instance.isGagal == false)
        {
            StartCoroutine(CalculateStar());
        }
        else if (PlayerData.Instance.isGagal == true) {
            StartCoroutine(ActivateNextAndBack());
        }
    }

    IEnumerator CalculateStar() {
        int score = gameObject.GetComponent<GameLogic>().totalScore;
        int gradeCount = 0;
        if (score >= 10)
        {
            gradeCount = 3;
        }
        else if (score >= 5) {
            gradeCount = 2;
        }
        else {
            gradeCount = 1;
        }

       for (int i = 0; i < gradeCount; i++)
            {
            float timer = 0f;
                while (timer < 1.4f)
                {
                    yield return new WaitForSeconds(0.01f);
                    bintang[i].GetComponent<Image>().fillAmount += Time.deltaTime;
                    timer += Time.deltaTime;
                }
            }
        StopCoroutine(CalculateStar());
        StartCoroutine(ActivateNextAndBack());
    }

    IEnumerator ActivateNextAndBack() {
        yield return new WaitForSeconds(0.5f);
        btnBack.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        PlayerData.Instance.health = 100;
        if (PlayerData.Instance.isGagal == false) { 
        btnCon.SetActive(true);
        }
        loading.SetActive(false);
        StopCoroutine(ActivateNextAndBack());
        StopAllCoroutines();
    }

    public void lookAtMenu(Transform menuTrans)
    {
        Camera.main.transform.position = new Vector3(menuTrans.position.x, menuTrans.position.y, 0);
    }


    public void nextLevel() {
        PlayerData.Instance.isGagal = false;
        StartCoroutine(videoDur(lvl));
        //SceneManager.LoadScene(lvl);
    }

    public void selectLevel() {
        PlayerData.Instance.isGagal = false;
        StartCoroutine(videoDur("scene_02"));
        //SceneManager.LoadScene("scene_00");
    }

    IEnumerator videoDur(string lvl)
    {
        Debug.Log("start play");
        float rand = Random.Range(2, 5);
        gameObject.GetComponent<MoviePlayer>().StartPlay();
        yield return new WaitForSeconds(1f);
        gameObject.GetComponent<GameLogic>().canvas.SetActive(false);
        img.SetActive(false);
        scoring.SetActive(false);
        yield return new WaitForSeconds(rand);
        SceneManager.LoadScene(lvl);
        //gameObject.GetComponent<MoviePlayer>().stopPlay();
        StopCoroutine(videoDur(lvl));
    }
}






