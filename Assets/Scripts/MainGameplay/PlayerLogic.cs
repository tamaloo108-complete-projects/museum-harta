﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLogic : MonoBehaviour
{
    public int scoreTot;
    public GameObject manager;
    public int maxHp;
    public int curHp;
    public Vector3 startPos;
    public bool invincible = false;
    public GameObject HPBar;
    private Transform startPosRotation;

    private void Awake()
    {
        
        //curHp = PlayerData.Instance.health;

        // startPosRotation.rotation = transform.rotation;
    }

    private void Start()
    {
        //curHp = PlayerData.Instance.health;
        PlayerData.Instance.isGagal = false;
        curHp = maxHp;
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Collectibles") && manager.GetComponent<GameLogic>().isPlaying)
        {
            Debug.Log("tertubruck");
            addScore();
            Destroy(col.gameObject);
        }
    }

    private void OnTriggerStay(Collider col)
    {
        if (col.gameObject.CompareTag("EndLine") && manager.GetComponent<GameLogic>().isPlaying && invincible == false)
        {
            gameObject.transform.position = startPos;
            Debug.Log("restart position");
            loseHealth(1);
            StartCoroutine(invincibility());
            // Debug.Log("tertabrack");
            //loseHealth(20);
            // StartCoroutine(invincibility());
        }
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Obstacle") && manager.GetComponent<GameLogic>().isPlaying && invincible == false)
        {
            gameObject.transform.position = startPos;
            loseHealth(1);
            StartCoroutine(invincibility());
            //gameObject.transform.Translate(startPos * Time.deltaTime);
            //gameObject.transform.rotation = startPosRotation.rotation;
        }
    }

    private void addScore()
    {
        int CurrentScore = manager.GetComponent<GameLogic>().score;
        scoreTot += CurrentScore;
        //int total = PlayerData.Instance.getScore() + CurrentScore;
        //    PlayerData.Instance.setScore(total);
        manager.GetComponent<GameLogic>().totalScore = scoreTot;
        manager.GetComponent<GameLogic>().updateTextScore();
    }

    private void loseHealth(int nil)
    {
       
        Debug.Log(curHp);
        if (curHp >= 0 && manager.GetComponent<GameLogic>().isPlaying)
        {
            
            HPBar.GetComponent<Animator>().SetInteger("hpStat", curHp);
            PlayerData.Instance.setHealth(curHp);
            curHp -= nil;

        }
        else if (curHp < 0) {
            PlayerData.Instance.isGagal = true;
            manager.GetComponent<GameLogic>().isPlaying = false;
            StartCoroutine(waitTime());
            if (manager.GetComponent<GameLogic>().firstTime == 0)
            {
                manager.GetComponent<postGame>().postGameStart();
                manager.GetComponent<GameLogic>().firstTime++;
                gameObject.transform.position = startPos;
                gameObject.transform.Translate(startPos * Time.deltaTime);
            }
            Debug.Log("game over");
        }
        //if (curHp > 6 && manager.GetComponent<GameLogic>().isPlaying)
        //{
        //    float Health = PlayerData.Instance.getHealth() - nil;

        //    //float currentHealth = mapHealth(Health, 0, maxHp, 0, 1);
        //    //Debug.Log(currentHealth);
        //    //PlayerData.Instance.setHealth(int.Parse(Health.ToString()));
        //    //manager.GetComponent<GameLogic>().curNilHp = currentHealth;
        //    //manager.GetComponent<GameLogic>().updateHPbar();
        //    curHp = Health;
        //}
        //else if (curHp <= 0) {
        //    PlayerData.Instance.isGagal = true;
        //    StartCoroutine(waitTime());
        //    //if (manager.GetComponent<GameLogic>().firstTime == 0) {
        //    //manager.GetComponent<postGame>().postGameStart();
        //    //manager.GetComponent<GameLogic>().firstTime++;
        //    //gameObject.transform.position = startPos;
        //    ////gameObject.transform.Translate(startPos * Time.deltaTime);
        //    //}
        //    Debug.Log("game over");
        //}
    }

    private float mapHealth(float curHealth, float minHealth, float maxHealth, float minBar, float maxBar)
    {
        return (curHealth) * (maxBar - minBar) / (maxHealth - minHealth) + minBar;

    }


    IEnumerator invincibility()
    {
        StopCoroutine(invincibility());
        if (!invincible)
        {
            gameObject.transform.position = startPos;
            invincible = true;
            gameObject.GetComponent<Animator>().speed = 2;
            Time.timeScale = 0.5f;
            Debug.Log("mulai kebal");
            gameObject.GetComponent<Animator>().SetInteger("hit", 1);
            yield return new WaitForSeconds(1f);
            gameObject.GetComponent<Animator>().speed = 1;
            Time.timeScale = 1f;
            gameObject.GetComponent<Animator>().SetInteger("hit", 0);
            yield return new WaitForSeconds(1.5f);
            Debug.Log("kebal selesai");
            //  yield return new WaitForSeconds(0.5f);
            invincible = false;
        }
        StopCoroutine(invincibility());
    }


    IEnumerator waitTime()
    {
        StopCoroutine(waitTime());
        manager.GetComponent<GameLogic>().introPlaying = true;
        gameObject.GetComponent<Animator>().SetBool("end", true);
        yield return new WaitForSeconds(3.5f);

        manager.GetComponent<GameLogic>().isPlaying = false;
        if (manager.GetComponent<GameLogic>().firstTime == 0)
        {
           // PlayerData.Instance.setHealth(5);
           // PlayerData.Instance.health = 5;
            manager.GetComponent<postGame>().postGameStart();
            manager.GetComponent<GameLogic>().firstTime++;
            gameObject.transform.position = startPos;

            //gameObject.transform.Translate(startPos * Time.deltaTime);
        }

        //manager.GetComponent<GameLogic>().introPlaying = false;
        StopCoroutine(waitTime());
    }

    IEnumerator hit()
    {
        StartCoroutine(hit());
        StopCoroutine(hit());

        yield return new WaitForSeconds(1.3f);
        gameObject.GetComponent<Animator>().SetInteger("hit", 1);
        yield return new WaitForSeconds(0.3f);
        gameObject.GetComponent<Animator>().SetInteger("hit", 0);

        StopCoroutine(hit());
    }

}
