﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    public float speed;
    public GameObject manager;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (manager.GetComponent<GameLogic>().introPlaying == false) {     
            if (manager.GetComponent<GameLogic>().isPlaying) { 
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Translate(Vector3.left*Time.deltaTime* speed,Space.World);
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.Translate(Vector3.right * Time.deltaTime* speed, Space.World);
            }
	        }
        }
    }
}
