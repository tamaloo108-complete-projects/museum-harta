﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPoints : MonoBehaviour {
    [SerializeField]public static Transform[] StartPoint;
    [SerializeField]public static Transform[] EndPoint;
    public GameObject _start, _end;
	// Use this for initialization
	void Awake () {
        StartPoint = new Transform[_start.transform.childCount];
        EndPoint = new Transform[_end.transform.childCount];

        for (int i = 0; i < StartPoint.Length;i++) {
            StartPoint[i] = _start.transform.GetChild(i);
        }
        for (int i = 0; i < EndPoint.Length; i++)
        {
            EndPoint[i] = _end.transform.GetChild(i);
        }
    }
	
	
}
