﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPillar : MonoBehaviour {

    public Transform enemyPrefab;
    public Transform[] spawnPoint;
    private float countdown = 2f;
    public float timeBetweenWaves = 5f;
    public int waveIndex;
    public int index;
    public GameObject manager;
    public GameObject enemy;
    // Update is called once per frame
    void Update()
    {
        if (manager.GetComponent<GameLogic>().introPlaying == false)
        {
            if (manager.GetComponent<GameLogic>().isPlaying)
            {

                if (countdown <= 0f)
                {
                    //release the kraken!
                    StartCoroutine(spawnWave());
                    countdown = timeBetweenWaves;

                }
                countdown -= Time.deltaTime;
            }
        }
    }

    IEnumerator spawnWave()
    {
        // waveIndex++;
        for (int i = 0; i < waveIndex; i++)
        {
            spawnEnemy();
            yield return new WaitForSeconds(0.4f);
        }


    }

    public void spawnEnemy()
    {
        index = 2;
        //int enemyIndex = Random.Range(0, enemyPrefab.Length);
        Instantiate(enemyPrefab, new Vector3(spawnPoint[index].position.x, enemyPrefab.position.y, spawnPoint[index].position.z), enemyPrefab.rotation);
        enemy.GetComponent<EnemyMovement>().num = index;
    }
}
