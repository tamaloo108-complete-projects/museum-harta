﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PillarMovement : MonoBehaviour {

    public float speed = 10f;
    private Transform target;
    public int num;
    private int wavePointIndex;
    public GameObject manager;
    void Start()
    {
        //target = WayPoints.EndPoint[UnityEngine.Random.Range(0,WayPoints.EndPoint.Length)];
        manager = GameObject.FindGameObjectWithTag("Manager");
        target = GameObject.FindGameObjectWithTag("PillarEnd").transform;
       // setTarget(2);
    }

    // Update is called once per frame
    void Update()
    {
        if (manager.GetComponent<GameLogic>().introPlaying == false)
        {
            Vector3 dir = target.position - transform.position;
            transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

            if (Vector3.Distance(transform.position, target.position) <= 0.3f)
            {
                dealMeleeDamage();
            }
        }
    }

    public void setTarget(int index)
    {
        //target = WayPoints.EndPoint[index];
    }

    private void dealMeleeDamage()
    {
        if (wavePointIndex >= WayPoints.EndPoint.Length)
        {
            Destroy(gameObject);
        }
        wavePointIndex++;
    }
}
