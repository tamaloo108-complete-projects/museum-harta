﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadPoints : MonoBehaviour {

   [SerializeField] public static Transform startPos;
   [SerializeField] public static Transform endPos;
   public GameObject start, end;

    void Awake()
    {
        startPos = start.transform.GetChild(0);
        endPos = end.transform.GetChild(0);
        
    }

}
