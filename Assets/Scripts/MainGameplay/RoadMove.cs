﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadMove : MonoBehaviour {
    public float speed;
    private Transform target;
    private int count;
	// Use this for initialization
	void Start () {
        target = RoadPoints.endPos;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= 0.4f)
        {
            destroyObj();
        }
    }

    private void destroyObj()
    {
        if (count >= WayPoints.EndPoint.Length)
        {
            Destroy(gameObject);
        }
        count++;
    }
}

