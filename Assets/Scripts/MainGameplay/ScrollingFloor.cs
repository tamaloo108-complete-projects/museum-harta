﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingFloor : MonoBehaviour
{
    public float speed;
    public bool invert;
    public GameObject manager;

    private void Update()
    {
        if (manager.GetComponent<GameLogic>().introPlaying == false) {
        ScrollFloor();
        }
    }

    private void ScrollFloor()
    {
        Material x = GetComponent<MeshRenderer>().material;
        Vector2 loc = x.mainTextureOffset;
        if (!invert)
        {
            loc.y -= Time.deltaTime / speed;
        }
        else {
            loc.y += Time.deltaTime / speed;
        }
        x.mainTextureOffset = loc;
    }
}
