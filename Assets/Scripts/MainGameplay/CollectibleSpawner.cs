﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleSpawner : MonoBehaviour {
    public Transform[] collectible;
    public Transform[] spawnPoint;
    private float countdown = 0f;
    public float timeBetweenWaves;
    public int waveIndex;
    public GameObject manager;

    void Update()
    {
        if (gameObject.GetComponent<GameLogic>().introPlaying == false)
        {
            if (manager.GetComponent<GameLogic>().isPlaying)
            {
                if (countdown <= 0f)
                {
                    //release the kraken!
                    StartCoroutine(spawnWave());
                    countdown = timeBetweenWaves;

                }
                countdown -= Time.deltaTime;
            }
        }
    }

    IEnumerator spawnWave()
    {
        // waveIndex++;
        for (int i = 0; i < waveIndex; i++)
        {
            spawnEnemy();
            yield return new WaitForSeconds(0.4f);
        }


    }

    public void spawnEnemy()
    {
        int randomSpawn = Random.Range(0, collectible.Length);
        Instantiate(collectible[randomSpawn], spawnPoint[Random.Range(0,5)].position, spawnPoint[Random.Range(0,5)].rotation);

    }
}
