﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatueSpawner : MonoBehaviour {

    public Transform[] enemyPrefab;
    public Transform[] spawnPoint;
    private float countdown = 2f;
    public float timeBetweenWaves = 5f;
    public int waveIndex;
    public int index;
    public bool bossStage;
    public GameObject manager;
    public GameObject[] enemy;
    // Update is called once per frame

    private void Start()
    {
        if (bossStage) {
            for (int i = 0; i < enemyPrefab.Length; i++)
            {
                enemyPrefab[i].GetComponent<EnemyMovementV2>().speed = 12;
            }
            
        }
    }

    void Update()
    {
        if (gameObject.GetComponent<GameLogic>().introPlaying == false) { 
            if (manager.GetComponent<GameLogic>().isPlaying)
            {

            if (countdown <= 0f)
            {
                //release the kraken!
                StartCoroutine(spawnWave());
                countdown = timeBetweenWaves;

            }
            countdown -= Time.deltaTime;
            }
        }
    }

    IEnumerator spawnWave()
    {
        // waveIndex++;
        for (int i = 0; i < waveIndex; i++)
        {
            spawnEnemy();
            yield return new WaitForSeconds(0.4f);
        }


    }

    public void spawnEnemy()
    {
        index = Random.Range(0, 4);
        int enemyIndex = Random.Range(0, enemyPrefab.Length);
        Instantiate(enemyPrefab[enemyIndex], spawnPoint[index].position, enemyPrefab[enemyIndex].rotation);
        enemy[enemyIndex].GetComponent<EnemyMovementV2>().num = index;
    }
}
