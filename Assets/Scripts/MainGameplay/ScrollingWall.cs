﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingWall : MonoBehaviour {

    public Transform[] wallRight, wallLeft;
    public Transform targetLeft,targetRight;
    public int backgroundSize;
    public float speed;
    private Vector3 startPosLeft;
    private Vector3 startPosRight;
    private int rightIndex;
    private int leftIndex;
    

    private void Start()
    {

        startPosRight = wallRight[0].position;
        startPosLeft = wallLeft[0].position;
        //startPos.z -= backgroundSize;
    }

    private void Update()
    {
        //startPos = wall[0].position;
        if (gameObject.GetComponent<GameLogic>().introPlaying == false) { 
        ScrollRightWall();
        ScrollLeftWall();
        }
    }

    private void ScrollRightWall() {
        
        for (int i = 0; i < wallRight.Length; i++)
        {
            Vector3 dir = targetRight.position - wallRight[i].position;
            wallRight[i].transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

            if (Vector3.Distance(wallRight[i].position, targetRight.position) <= 0.1f) {
                wallRight[i].position = startPosRight;

            }
        }
    }

    private void ScrollLeftWall() {
        for (int i = 0; i < wallLeft.Length; i++)
        {
            Vector3 dir = targetLeft.position - wallLeft[i].position;
            wallLeft[i].transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

            if (Vector3.Distance(wallLeft[i].position, targetLeft.position) <= 0.1)
            {
                wallLeft[i].position = startPosLeft;

            }
        }
    }
}
