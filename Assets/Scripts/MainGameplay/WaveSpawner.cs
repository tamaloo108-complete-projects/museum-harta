﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour {

    public Transform enemyPrefab;
    public Transform[] spawnPoint;
    private float countdown = 2f;
    public float timeBetweenWaves = 5f;
    public int waveIndex;
    public int index;
    public GameObject manager, enemy;
    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponent<GameLogic>().introPlaying == false) { 
        if (manager.GetComponent<GameLogic>().isPlaying)
        {

            if (countdown <= 0f)
            {
                //release the kraken!
                StartCoroutine(spawnWave());
                countdown = timeBetweenWaves;

            }
            countdown -= Time.deltaTime;
        }
        }
    }

    IEnumerator spawnWave() {
       // waveIndex++;
        for (int i = 0; i < waveIndex; i++)
        {
            spawnEnemy();
            yield return new WaitForSeconds(0.4f);
        }

        
    }

    public void spawnEnemy()
    {
       index = Random.Range(0, 5);
       Instantiate(enemyPrefab, spawnPoint[index].position, spawnPoint[index].rotation);
       enemy.GetComponent<EnemyMovement>().num = index; 
    }
}
