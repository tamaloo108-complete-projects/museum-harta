﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour
{
    public bool isPlaying, introPlaying, deadPlaying, bossStage, Finale;
    public int score, firstTime = 0;
    public int totalScore, UnlockLevel;
    public Text scoreText;
    public AudioClip win, lose;
    // Use this for initialization
    public GameObject hp, maling, player, line, canvas, beluk;
    public float curNilHp;
    public float time, curTime, maxTime;
    public GameObject timerBar;

    private void Start()
    {
        introBoi(true);
    }


    public void introBoi(bool cek)
    {
        canvas.SetActive(false);
        maling.SetActive(true);
        introPlaying = cek;
        player.GetComponent<BoxCollider>().enabled = false;
        player.GetComponent<Rigidbody>().isKinematic = true;
        StartCoroutine(malingStart());
    }

    public void updateTextScore()
    {
        scoreText.text = totalScore.ToString();
    }

    //public void updateHPbar() {
    //    hp.GetComponent<Image>().fillAmount = curNilHp;
    //}

    void Update()
    {
        if (!introPlaying)
        {
            if (time <= 50 && isPlaying)
            {
                maxTime = PlayerData.Instance.getTimer();
                time += Time.deltaTime;
                //Debug.Log(Mathf.Floor(time));
                curTime = updateTimer(50, time, 0, 0, 1);
                StartCoroutine(curTimeUp());
            }
            else
            {
                isPlaying = false;
                StartCoroutine(waitTime());
                StopCoroutine(curTimeUp());
                StartCoroutine(playerWin());
            }
        }
    }

    private float updateTimer(float maxTime, float curTime, float minTime, float minBar, float maxBar)
    {
        return (curTime) * (maxBar - minBar) / (maxTime - minTime) + minBar;

    }

    IEnumerator curTimeUp()
    {
        StopCoroutine(curTimeUp());
        yield return new WaitForSeconds(1f);
        timerBar.GetComponent<Image>().fillAmount = curTime;
    }

    IEnumerator waitTime()
    {
        StopCoroutine(waitTime());
        yield return new WaitForSeconds(4.5f);
    }

    IEnumerator malingStart()
    {
        StopCoroutine(malingStart());
        float timer = 4.5f;
        float timer2 = 3.4f;
        while (timer > 0f)
        {
            yield return new WaitForSeconds(0.01f);
            maling.transform.Translate(new Vector3(0, 0, 5f) * Time.deltaTime);
            timer -= Time.deltaTime;
        }
        //yield return new WaitForSeconds(1.5f);
        maling.GetComponent<Animator>().SetInteger("tunggu", 1);
        yield return new WaitForSeconds(0.7f);
        maling.GetComponent<Animator>().SetInteger("tunggu", 0);
        yield return new WaitForSeconds(0.5f);
        while (timer2 > 0f)
        {
            yield return new WaitForSeconds(0.01f);
            maling.transform.Translate(new Vector3(0, 0, 5f) * Time.deltaTime);
            timer2 -= Time.deltaTime;
        }
        maling.SetActive(false);
        StartCoroutine(playerOnStart());
        StopCoroutine(malingStart());
        //StopCoroutine(playerOnStart());
    }

    IEnumerator playerOnStart()
    {
        StopCoroutine(playerOnStart());
        line.SetActive(false);
        Debug.Log("we start player");
        float timer = 2.5f;
        Vector3 pos = new Vector3(0, 0, 4f);
        while (timer > 0f)
        {
            yield return new WaitForSeconds(0.01f);
            player.transform.Translate(pos * Time.deltaTime);
            timer -= Time.deltaTime;
        }
        player.GetComponent<BoxCollider>().enabled = true;
        player.GetComponent<Rigidbody>().isKinematic = false;
        line.SetActive(true);
        canvas.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        introPlaying = false;
        StopCoroutine(playerOnStart());
    }

    IEnumerator playerWin()
    {
        if (!bossStage)
        {
            gameObject.GetComponent<AudioSource>().Stop();
            player.GetComponent<Animator>().SetInteger("win", 1);
            introPlaying = true;
            player.GetComponent<Rigidbody>().isKinematic = true;
            yield return new WaitForSeconds(3.5f);
            if (firstTime == 0)
            {
                if (PlayerData.Instance.isGagal == false)
                {
                    gameObject.GetComponent<AudioSource>().clip = win;
                    gameObject.GetComponent<AudioSource>().Play();
                    //PlayerData.Instance.UnlockNewLevel(UnlockLevel);
                    PlayerData.Instance.UnlockNewLevel(UnlockLevel);
                    PlayerData.Instance.levelAvailability = UnlockLevel;
                }
                gameObject.GetComponent<AudioSource>().Stop();
                gameObject.GetComponent<postGame>().postGameStart();
                firstTime++;
            }
            StopCoroutine(playerWin());
        }
        else if (bossStage)
        {
            gameObject.GetComponent<AudioSource>().clip = win;
            gameObject.GetComponent<AudioSource>().Play();
            float timer = 0f;
            introPlaying = true;
            player.GetComponent<Rigidbody>().isKinematic = true;
            maling.transform.position = new Vector3(-0.27f, 1.3f, -1.2f);
            maling.SetActive(true);
            Vector3 pos = new Vector3(0, 0, 4f);
            //Vector3 dir = maling.transform.position- player.transform.position;
            while (timer < 3.2f)
            {
                yield return new WaitForSeconds(0.01f);
                player.transform.position = Vector3.MoveTowards(player.transform.position, maling.transform.position, 3f * Time.deltaTime);
                timer += Time.deltaTime;
            }
            beluk.SetActive(true);
            player.SetActive(false);
            maling.SetActive(false);
            yield return new WaitForSeconds(2.5f);
            player.SetActive(true);
            maling.SetActive(true);
            maling.transform.position = new Vector3(1.67f, 1.3f, -1.2f);
            player.GetComponent<Animator>().SetInteger("win", 1);
            maling.GetComponent<Animator>().SetInteger("tangkep", 1);
            yield return new WaitForSeconds(0.1f);
            beluk.SetActive(false);

            yield return new WaitForSeconds(1.3f);
            if (firstTime == 0)
            {
                if (PlayerData.Instance.isGagal == false)
                {
                    PlayerData.Instance.UnlockNewLevel(UnlockLevel);
                    PlayerData.Instance.levelAvailability = UnlockLevel;
                }
                gameObject.GetComponent<AudioSource>().Stop();
                gameObject.GetComponent<postGame>().postGameStart();

                firstTime++;
            }
            StopCoroutine(playerWin());
        }
        else if (Finale && bossStage)
        {
            gameObject.GetComponent<AudioSource>().clip = win;
            gameObject.GetComponent<AudioSource>().Play();
            float timer = 0f;
            introPlaying = true;
            player.GetComponent<Rigidbody>().isKinematic = true;
            maling.transform.position = new Vector3(-0.27f, 1.3f, -1.2f);
            maling.SetActive(true);
            Vector3 pos = new Vector3(0, 0, 4f);
            //Vector3 dir = maling.transform.position- player.transform.position;
            while (timer < 3.2f)
            {
                yield return new WaitForSeconds(0.01f);
                player.transform.position = Vector3.MoveTowards(player.transform.position, maling.transform.position, 3f * Time.deltaTime);
                timer += Time.deltaTime;
            }
            beluk.SetActive(true);
            player.SetActive(false);
            maling.SetActive(false);
            yield return new WaitForSeconds(2.5f);
            player.SetActive(true);
            maling.SetActive(true);
            maling.transform.position = new Vector3(1.67f, 1.3f, -1.2f);
            player.GetComponent<Animator>().SetInteger("win", 1);
            maling.GetComponent<Animator>().SetInteger("tangkep", 1);
            yield return new WaitForSeconds(0.1f);
            beluk.SetActive(false);

            yield return new WaitForSeconds(1.3f);
            if (firstTime == 0)
            {
                if (PlayerData.Instance.isGagal == false)
                {
                    PlayerData.Instance.UnlockNewLevel(UnlockLevel);
                    PlayerData.Instance.levelAvailability = UnlockLevel;
                }
                gameObject.GetComponent<AudioSource>().Stop();
                //gameObject.GetComponent<postGame>().postGameStart();
                StartCoroutine(gameObject.GetComponent<postGame>().finaleGame());

                firstTime++;
            }
            StopCoroutine(playerWin());
        }
    }
}
