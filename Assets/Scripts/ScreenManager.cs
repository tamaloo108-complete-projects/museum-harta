﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ScreenManager : MonoBehaviour
{
    public List<int> usedNum = new List<int>();
    public GameObject cardGO;
    public GameObject parentCardGO;
    public int min, max, length, score, num, first_img, second_img;
    string cardLength, cardIdentity;

    public int counter;

    public SpriteState buttonMatch = new SpriteState();
    // Use this for initialization
    void Start()
    {
        counter = 0;
        loadCard();
    }


    private void loadCard()
    {

        Sprite[] _cardTexture = Resources.LoadAll<Sprite>("the_cards");
        for (int i = 0; i <= length; i++)
        {
            GameObject card = Instantiate(cardGO) as GameObject;
            GameObject obj = card.gameObject.transform.GetChild(0).gameObject;
            card.transform.SetParent(parentCardGO.transform, false);
            num = uniqueRandomizer(min, max);
            cardLength = num.ToString();
            if (cardLength.Length > 1)
            {
                cardIdentity = cardLength.Substring(1, 1);
            }
            else
            {
                cardIdentity = cardLength;
            }
            obj.GetComponent<Image>().sprite = _cardTexture[num];
            card.GetComponent<CardFlag>().cardIndex = num;
            card.GetComponent<CardFlag>().cardIdentifier = int.Parse(cardIdentity);
            obj.GetComponent<Image>().enabled = false;
            card.GetComponent<Button>().onClick.AddListener(() => ClickMatch(card));
        }
    }

    private int uniqueRandomizer(int min, int max)
    {
        int val = Random.Range(min, max);
        if (usedNum.Contains(val))
        {
            return uniqueRandomizer(min, max);
        }
        else if (!usedNum.Contains(val))
        {
            usedNum.Add(val);
        }
        return val;
    }

    private void ClickMatch(GameObject obj)
    {
        Debug.Log("lul");
        GameObject x = obj.gameObject.transform.GetChild(0).gameObject;
        if (counter == 0)
        {
            x.gameObject.GetComponent<Image>().enabled = true;
            first_img = obj.GetComponent<CardFlag>().cardIdentifier;
            counter++;
        }
        else if (counter == 1 && x.GetComponent<Image>().enabled == false)
        {
            x.gameObject.GetComponent<Image>().enabled = true;
            second_img = obj.GetComponent<CardFlag>().cardIdentifier;
            bestMatch();
            counter = 0;
        }

    }

    private void bestMatch()
    {
        bool cari = true;

        while (cari)
        {
            GameObject[] allCard = GameObject.FindGameObjectsWithTag("Card");
            foreach (GameObject nine in allCard)
            {

            }
        }
    }
}




