﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragPieces : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

    public static GameObject item;
    public Vector3 startPos;
    public Transform parentPos;
    public int index;
    public GameObject obj;

    void Start()
    {
        obj = GameObject.FindGameObjectWithTag("Main").gameObject;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        item = gameObject;
        startPos = transform.position;
        parentPos = transform.parent;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 posBoi = Input.mousePosition;
        transform.position = new Vector3(posBoi.x, posBoi.y, startPos.z);
        obj.GetComponent<PuzzleLogic>().curIndex = index;
    }


    public void OnEndDrag(PointerEventData eventData)
    {


        item = null;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        if (transform.parent == parentPos)
        {
            transform.position = startPos;
        }
        //obj.GetComponent<SlotPieces>().currentIndex = index;
    }


}
