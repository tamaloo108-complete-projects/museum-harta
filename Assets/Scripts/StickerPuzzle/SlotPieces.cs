﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SlotPieces : MonoBehaviour, IDropHandler {

    public int slotIndex;
    public int currentIndex;
    public GameObject mainContainer;

    public GameObject item {
        get {
            if (transform.childCount > 0) {
                return transform.GetChild(0).gameObject;
            }
            return null;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        
        if (!item) {
            DragPieces.item.transform.SetParent(transform);
            GetCurNum();
            if (slotIndex == currentIndex)
            {
                mainContainer.GetComponent<PuzzleLogic>().answer[slotIndex] = 1;
            }
            else {
                mainContainer.GetComponent<PuzzleLogic>().answer[slotIndex] = 0;
            }
            
        }
    }

    public void GetCurNum() {
       // Debug.Log(transform.GetComponentInChildren<DragPieces>().index);
        currentIndex = transform.GetComponentInChildren<DragPieces>().index;
        Debug.Log(currentIndex);
    }
}
