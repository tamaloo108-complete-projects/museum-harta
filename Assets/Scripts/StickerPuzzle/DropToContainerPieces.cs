﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropToContainerPieces : MonoBehaviour, IDropHandler {

    public GameObject winPanel;
    public bool winYay = false;
    public GameObject mainContainer;
    public GameObject item
    {
        get
        {
            return transform.gameObject;
        }
    }

    void Start()
    {
        mainContainer = GameObject.FindGameObjectWithTag("Main").gameObject;
    }

    public void OnDrop(PointerEventData eventData)
    {
        DragPieces.item.transform.SetParent(transform);
        mainContainer.GetComponent<PuzzleLogic>().answer[getNumSlot()] = 0;
    }

    public int getNumSlot() {
        return mainContainer.GetComponent<PuzzleLogic>().curIndex;
    }

    void Update()
    {
        if (transform.childCount == 0) {
            winYay = mainContainer.GetComponent<PuzzleLogic>().winCondition();
        }

        if (winYay) {
            Debug.Log("win boi");
            winPanel.active = true;
        }
    }
}
