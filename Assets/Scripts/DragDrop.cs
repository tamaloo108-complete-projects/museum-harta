﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragDrop : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public static GameObject item;
    public GameObject currentItem;
    public Vector3 startPos;
    public Transform startParent;
    public int nil;

    public void OnBeginDrag(PointerEventData eventData)
    {
        item = gameObject;
        startParent = transform.parent;
        startPos = transform.position;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 posBoi = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(posBoi.x, posBoi.y, startPos.z);
        currentItem.GetComponent<SlotAnswer>().currentAnswer = nil;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        item = null;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        if (transform.parent == startParent)
        {
            // GetComponent<Image>().raycastTarget = false;
            transform.position = startPos;
        }
        // transform.position = startParent.position;
    }

    //void Update() {
    //    Debug.Log(Input.mousePosition);
    //}


    //public void gerak(GameObject item_) {

    //    Debug.Log("masuk");
    //    item_.transform.position = new Vector3(Input.mousePosition.x-Screen.width/2 , Input.mousePosition.y-Screen.height/2, Input.mousePosition.z);
    //}
}
