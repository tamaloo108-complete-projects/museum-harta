﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopMove : MonoBehaviour {

    public GameObject move;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        move.GetComponent<VisualEffect>().executeMove = false;
    }
}
