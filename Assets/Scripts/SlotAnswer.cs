﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SlotAnswer : MonoBehaviour, IDropHandler
{
    public int rightAnswer;
    public int currentAnswer;
    public int unlockedLevel;
    public GameObject effects;
    public GameObject bg, manager;
    public string level, main;
    public GameObject item
    {
        get
        {
            if (transform.childCount > 0)
            {
                return transform.GetChild(0).gameObject;
            }
            return null;
        }
    }


    public void OnDrop(PointerEventData eventData)
    {
        if (!item && rightAnswer == currentAnswer)
        {
            Debug.Log("current : " + currentAnswer);
            Debug.Log("yee bener");
            DragDrop.item.transform.SetParent(transform);
            effects.GetComponent<VisualEffect>().executeMove = true;
            //PlayerData.Instance.UnlockNewLevel(1);
            //loadLevel("scene_00");
            StartCoroutine(tunggu());


        }
    }

    private void miniGameCounter(int nil)
    {
        if (PlayerData.Instance.miniGameCounter == 1)
        {

            // PlayerData.Instance.setMiniGame(2);
            // PlayerData.Instance.miniGameCounter = 2;
            StartCoroutine(blankScreen());
            //loadLevel(main);
            //loadLevel(main);
        }
        else
        {
            SceneManager.LoadScene(level);
        }
        PlayerData.Instance.miniGameCounter -= nil;
    }
    private void loadLevel(string lvl)
    {
        // StartCoroutine(videoDur(lvl));
        //SceneManager.LoadScene(lvl);
        StartCoroutine(loadTheScreen(lvl));
    }

    IEnumerator loadTheScreen(string lvl)
    {
        manager.GetComponent<MoviePlayer>().StartPlay();
        yield return new WaitForSeconds(3f);
        AsyncOperation loading = SceneManager.LoadSceneAsync(lvl);
        float prog = loading.progress;
        while (prog < 1)
        {
            yield return new WaitForSeconds(0.01f);
            Debug.Log(prog);
        }
        yield return new WaitForSeconds(3f);
        manager.GetComponent<MoviePlayer>().stopPlay();
        StopCoroutine(loadTheScreen(lvl));
    }

    IEnumerator tunggu()
    {
        //bg.SetActive(true);
        yield return new WaitForSeconds(1.3f);
        //while (bg.GetComponent<Image>().fillAmount <= 1) { 
        //bg.GetComponent<Image>().fillAmount += Time.deltaTime;
        //}
        //bg.SetActive(false);
        miniGameCounter(1);
        StopCoroutine(tunggu());
    }

    IEnumerator blankScreen()
    {
        //StopCoroutine(blankScreen());

        bg.SetActive(true);
        Debug.Log("jalan");
        while (bg.GetComponent<Image>().fillAmount != 1)
        {
            yield return new WaitForSeconds(0.01f);
            bg.GetComponent<Image>().fillAmount += Time.deltaTime;
        }
        //StopCoroutine(blankScreen());
        PlayerData.Instance.setMiniGame(2f);
        PlayerData.Instance.miniGameCounter = 2;
        yield return new WaitForSeconds(0.5f);
        //StopCoroutine(destroyBlankScreen());
        //StartCoroutine(videoDur(main));
        StartCoroutine(loadTheScreen(main));
        yield return new WaitForSeconds(1.5f);
        //loadLevel(main);
        //artCoroutine(destroyBlankScreen());
        //loadLevel(main);
        //miniGameCounter(1);
    }

    IEnumerator destroyBlankScreen()
    {
        StopCoroutine(destroyBlankScreen());
        Debug.Log("jalan lagi");
        while (bg.GetComponent<Image>().fillAmount != 0)
        {
            yield return new WaitForSeconds(0.01f);
            bg.GetComponent<Image>().fillAmount -= Time.deltaTime;
        }
        bg.SetActive(false);

    }
}
