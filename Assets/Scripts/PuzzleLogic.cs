﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PuzzleLogic : MonoBehaviour
{
    public string[] jigsawName;
    public GameObject piece;
    public Transform parentPos;
    public List<GameObject> _pieceList;
    [HideInInspector]public List<Sprite> imgList;
    public int curIndex;
    public int unlockedLevel;
    public int[] answer;
    public bool win;
    private int puzzleCount;
    public string lvl, menu;
    // Use this for initialization
    void Start()
    {
        //PlayerPrefs.SetInt("MiniGameCounter", 2);
        puzzleCount = 2;
        answer = new int[9];
        loadPieces();
    }

    public void loadPieces()
    {
        int num = 0;
        int rand = Random.Range(0, jigsawName.Length);
 
        Sprite[] _img = Resources.LoadAll<Sprite>(jigsawName[rand]);
        foreach (Sprite img in _img)
        {
            GameObject x = Instantiate(piece, parentPos, false);
            _pieceList.Add(x);
            imgList.Add(img);
            num++;
        }

        randomImgIndex(imgList, _pieceList);
    }

    private void randomImgIndex(List<Sprite> img, List<GameObject> obj)
    {
        int count = 0;
        int num = 0;
        List<Sprite> x = img;

        for (int i = 0; i < obj.Count; i++)
        {
            num = Random.Range(0, img.Count);
            _pieceList[i].GetComponent<Image>().sprite = img[num];
            _pieceList[i].GetComponent<DragPieces>().index = int.Parse(img[num].name);
            Debug.Log(num);
            x.RemoveAt(num);
        }
    }

    public bool winCondition() {
        int check = 0;
        for (int i = 0; i < answer.Length; i++)
        {
            if (answer[i] == 1) {
                check++;
            }
        }

        if (check >= 9) {
             win = true;
            puzzleCount++;
        }
        else {
            win = false;
        }
        return win;
    }

    public void next() {
        PlayerData.Instance.miniGameCounter -= 1;
        if (PlayerData.Instance.miniGameCounter == 0)
        {
          //  PlayerData.Instance.UnlockNewLevel(unlockedLevel);
            PlayerData.Instance.miniGameCounter = 2;
            SceneManager.LoadScene(menu);

        }
        else {
            SceneManager.LoadScene(lvl);
        }
    }
}
