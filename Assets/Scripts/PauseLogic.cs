﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseLogic : MonoBehaviour {

    public GameObject pause, pauseBtn;
    public bool isPause;
    private void Start()
    {
       // isPause = false;
    }

    public void pauseControl() {
        
        if (!isPause)
        {
            isPause = true;
            pause.SetActive(true);
            pauseBtn.SetActive(false);
            Time.timeScale = 0;

        }
        else {
            pause.SetActive(false);
            pauseBtn.SetActive(true);
            Time.timeScale = 1;
            isPause = false;
        }

    }


    public void returnMnu() {
        Time.timeScale = 1;
        SceneManager.LoadScene("scene_02");

    }
}
